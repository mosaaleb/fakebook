
# :computer: Devium

## Intro
This is rails clone to facebook.

## Live Version
The live version of this project is [Devium](https://deviumio.herokuapp.com/).

## Database Design
[ERD](https://www.lucidchart.com/invitations/accept/02f604cf-c6bb-4e94-a595-4b3c6856d8a3)

## Features
- Sending friend requests.
- Accepting friend requests.
- Remove friends.
- Add posts.
- Add comment to posts.
- Having newsfeed page based on the friends list.
- Ability to login with facebook.
- Edit setting and profile information.

## Contributors
- [Saheed](https://github.com/suretrust)
- [Muhammad](https://github.com/mosaaleb)

